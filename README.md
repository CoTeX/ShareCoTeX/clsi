liederboek/overleaf/clsi
===============

Fork of Overleaf's CLSI, a web api for compiling LaTeX documents in the cloud.

Overleaf's official repository can be found on GitHub:
https://github.com/overleaf/clsi

Installation
------------

The CLSI can be installed and set up as part of the entire [Liederboek Overleaf stack](https://gitlab.com/liederboek/overleaf) (complete with front end editor and document storage), or it can be run as a standalone service. To run is as a standalone service, first checkout this repository:

    $ git clone git@gitlab.com:liederboek/overleaf/clsi.git
    
Then install the require npm modules:

    $ npm install

Finally run the CLSI service:

    $ npm start
    
The CLSI should then be running at http://localhost:3013.

License
-------

The code in this repository is released under the GNU AFFERO GENERAL PUBLIC LICENSE, version 3. A copy can be found in the `LICENSE` file.

Copyright (c) Overleaf, 2014-2019.
